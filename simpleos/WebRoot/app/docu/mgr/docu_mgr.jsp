<%@page import="net.simpleos.SimpleosUtil"%>
<%@page import="net.simpleos.utils.StringsUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page import="java.util.Map"%><%@page
	import="net.simpleos.backend.BackendUtils"%>

<%
	Map<String, String> map = SimpleosUtil.attrMap;
%>
<div style="padding: 0px 12px;">
	<p>
		<input type="button" onclick="$Actions['docuMgrCatalogWindowAct']();"
			value="#(Docu.mgr.2)" />
	</p>
	<p>
		<input type="button" value="#(Docu.mgr.3)" /> <span
			id="span_docuReBuildStatData" class="important-tip"
			style="margin-left: 6px;"></span>
	</p>
	<p>
		<input type="button" value="#(Docu.mgr.4)" /> <span
			id="span_docuIndexRebuild" class="important-tip"
			style="margin-left: 6px;"></span>
	</p>
	<table id="docuMgrId">
		<tr>
			<td>存储路径:</td>
			<td><input type="text"
				value="<%=StringsUtils.trimNull(map.get("docu.docu_docuPath"), "c:\\")%>"
				name="docu_docuPath" id="docu_docuPath"></td>
		</tr>
		<tr>
			<td>单文件大小限制(M):</td>
			<td><input type="text"
				value="<%=StringsUtils.trimNull(map.get("docu.docu_fileSize"), "10")%>"
				name="docu_fileSize" id="docu_fileSize"></td>
		</tr>
		<tr>
			<td>可以解析的文件类型:</td>
			<td><input type="text"
				value="<%=StringsUtils.trimNull(map.get("docu.docu_fileExts"), "txt,properties,xml,java")%>"
				name="docu_fileExts" id="docu_fileExts"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="button" value="保存设置" class="button2"
				onclick="$IT.A('docuPathAct');"></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	(function() {
		function init(act) {
			var info = $("span_" + act);
			info.previous().observe("click", function() {
				info.innerHTML = "#(manager_tools.7)";
				$Actions[act]();
			});
		}
		init("docuReBuildStatData");
		init("docuIndexRebuild");
	})();
</script>